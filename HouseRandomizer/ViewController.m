//
//  ViewController.m
//  HouseRandomizer
//
//  Created by Pavel Ivanov on 17/03/15.
//  Copyright (c) 2015 paulchelly. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic, strong) NSArray *moves;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.moves = @[@"Heel Step",
                   @"Side walk",
                   @"Cross step",
                   @"Cross step с хитрым поворотом",
                   @"Setup",
                   @"Freeway",
                   @"Salsa step",
                   @"Salsa hop",
                   @"Hurtle",
                   @"Stomp",
                   @"Pas de Boure",
                   @"Swirl",
                   @"Cross legs",
                   @"Loose legs",
                   @"Farmer"];
}

- (IBAction)randomizeTap:(id)sender {
    NSMutableArray *selectedmoves = [NSMutableArray array];
    while (selectedmoves.count < 3) {
        NSInteger randomIndex = rand()%self.moves.count;
        NSString *move = self.moves[randomIndex];
        if (![selectedmoves containsObject:move]) {
            [selectedmoves addObject:move];
        }
    }
    
    self.moveLabel1.text = selectedmoves[0];
    self.moveLabel2.text = selectedmoves[1];
    self.moveLabel3.text = selectedmoves[2];
}

@end
