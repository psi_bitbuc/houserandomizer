//
//  ViewController.h
//  HouseRandomizer
//
//  Created by Pavel Ivanov on 17/03/15.
//  Copyright (c) 2015 paulchelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *moveLabel1;
@property (strong, nonatomic) IBOutlet UILabel *moveLabel2;
@property (strong, nonatomic) IBOutlet UILabel *moveLabel3;

- (IBAction)randomizeTap:(id)sender;

@end

